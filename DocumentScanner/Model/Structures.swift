//
//  Structures.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit

struct EditItem : Hashable{
    var name : String
    var icon : String
    var type : EditItemType
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    static func == (lhs: EditItem, rhs: EditItem) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    private let identifier = UUID()
    
}

struct ImageModel {
    var image : URL
    var signature : SignatureModel = SignatureModel()
    var filter : FilterModel = FilterModel()
    var texts : [TextModel] = []
    var draws : [DrawModel] = []
    var highlights : [HighlightModel] = []
}

//Text
struct TextModel : Hashable{
    var text : String = ""
    var textColor : UIColor = .black
    var fillColor : UIColor = .clear
    var fontSize : CGFloat = 22
    var fontName : String = UIFont.init().fontName
    var alignment : NSTextAlignment = .center
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    static func == (lhs: TextModel, rhs: TextModel) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    let identifier = UUID()
}

struct DrawModel : Hashable{
    var color : UIColor = UIColor.black
    var thikness : CGFloat = 22
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    static func == (lhs: DrawModel, rhs: DrawModel) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    let identifier = UUID()
}

struct HighlightModel : Hashable {
    var color : UIColor = UIColor.black
    var opacity : CGFloat = 0.5
    var thikness : CGFloat = 22
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    static func == (lhs: HighlightModel, rhs: HighlightModel) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    private let identifier = UUID()
}

struct FilterModel {
    var filterType : FilterType = .Original
    var brightness : Filter = Filter(type: .Brightness, defult: 0, minimum: 0, maximum: 1, current: 0)
    var sharpen : Filter = Filter(type: .Sharpen, defult: 0, minimum: 0, maximum: 1, current: 0)
    var contrast : Filter = Filter(type: .Contrast, defult: 0, minimum: 0, maximum: 1, current: 0)
}
struct Filter {
    var type : ImageEditType
    var defult : CGFloat
    var minimum : CGFloat
    var maximum : CGFloat
    var current : CGFloat
    
}

struct SignatureModel : Hashable {
    var image : UIImage? = nil
    var parent : CGRect = .zero
    var child : CGRect = .zero
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    static func == (lhs: SignatureModel, rhs: SignatureModel) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    let identifier = UUID()
}
