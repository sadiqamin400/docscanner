//
//  Enums.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import Foundation

enum EditItemType : Hashable {
    case Add
    case Sign
    case Text
    case Draw
    case Highlight
    case Filter
    case More
}

enum FilterType {
    case Original
    case BandW
    case Grayscale
    case Contrast
}
enum ImageEditType {
    case Brightness
    case Sharpen
    case Contrast
}
