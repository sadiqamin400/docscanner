//
//  DataLoader.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit

let kSignatureFolder = FileUtility.shared.defaultPath.appendingPathComponent("Signature")

class DataLoader: NSObject {

    static let shared = DataLoader()
    private override init() {
        
    }
    
    func docEditItems() -> [EditItem]{
        var array : [EditItem] = []
        array.append(EditItem(name: NSLocalizedString("Add", comment: ""), icon: "Add-Button", type: .Add))
        array.append(EditItem(name: NSLocalizedString("Sign", comment: ""), icon: "Sign-Button", type: .Sign))
        array.append(EditItem(name: NSLocalizedString("Text", comment: ""), icon: "Text-Button", type: .Text))
        array.append(EditItem(name: NSLocalizedString("Draw", comment: ""), icon: "Draw-Button", type: .Draw))
        array.append(EditItem(name: NSLocalizedString("Highlight", comment: ""), icon: "Highlight-Button", type: .Highlight))
        array.append(EditItem(name: NSLocalizedString("Filter", comment: ""), icon: "Filter-Button", type: .Filter))
        array.append(EditItem(name: NSLocalizedString("More", comment: ""), icon: "More-Button", type: .More))
        return array
    }
    
    func getAllSignature()-> [File]{
        
        return FileUtility.shared.scanDirectory(directory: kSignatureFolder)
    }
}
