//
//  SPGripViewBorderView.swift
//  ScrapBook
//
//  Created by Solution Cat Sadik on 29/12/19.
//  Copyright © 2019 Solution Cat Sadik. All rights reserved.
//

import UIKit

let kSPUserResizableViewGlobalInset = 5.0
let kSPUserResizableViewDefaultMinWidth = 48.0
let kSPUserResizableViewDefaultMinHeight = 48.0
let kSPUserResizableViewInteractiveBorderSize = 10.0

class SPGripViewBorderView: UIView {

    var borderColor : UIColor = UIColor.blue
    var borderWidth : CGFloat = 2.0
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.borderColor = UIColor.blue
        self.borderWidth = 2.0
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context!.saveGState()
        context!.setLineWidth(self.borderWidth)
        context!.setLineDash(phase: 1, lengths: [3,7])
        context!.setStrokeColor(self.borderColor.cgColor);
        context!.addRect(self.bounds.insetBy(dx: CGFloat(kSPUserResizableViewInteractiveBorderSize/2), dy: CGFloat(kSPUserResizableViewInteractiveBorderSize/2)));
        context!.strokePath();
        
        context!.restoreGState();
    }
}
