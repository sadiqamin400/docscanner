//
//  ZDStickerView.swift
//  ScrapBook
//
//  Created by Solution Cat Sadik on 29/12/19.
//  Copyright © 2019 Solution Cat Sadik. All rights reserved.
//

import UIKit
import QuartzCore

enum ZDStickerViewButton : Int  {
    typealias RawValue = Int
    case ZDStickerViewButtonNull = 0
    case ZDStickerViewButtonDel = 1
    case ZDStickerViewButtonResize = 2
    case ZDStickerViewButtonCustom = 3
    case ZDStickerViewButtonMax = 4
 
}


@objc protocol ZDStickerViewDelegate: NSObjectProtocol {
    @objc optional func stickerViewDidBeginEditing(sticker: JRStickerView?)
    @objc optional func stickerViewDidEndEditing(sticker: JRStickerView?)
    @objc optional func stickerViewDidCancelEditing(sticker: JRStickerView?)
    @objc optional func stickerViewDidClose(sticker: JRStickerView?)
    @objc optional func textStickerDoubleClick(textSticker : JRStickerView?)
#if ZDSTICKERVIEW_LONGPRESS
    @objc optional func stickerViewDidLongPressed(sticker: JRStickerView?)
#endif
    @objc optional func stickerViewDidCustomButtonTap(sticker: JRStickerView?)
}

class JRStickerView : UIView,UITextViewDelegate {
   
    let kSPUserResizableViewGlobalInset : CGFloat = 5.0
    let kSPUserResizableViewDefaultMinWidth : CGFloat = 48.0
    let kSPUserResizableViewInteractiveBorderSize : CGFloat = 10.0
    let kZDStickerViewControlSize : CGFloat = 24.0

    
    var stickerViewDelegate : ZDStickerViewDelegate?
    
    var contentView : UIView?
    // default = YES
    var preventsPositionOutsideSuperview : Bool = true
    // default = NO
    var preventsResizing : Bool = false
    // default = NO
    var preventsDeleting : Bool = false
    // default = YES
    var preventsCustomButton : Bool = true
    // default = YES
    var translucencySticker : Bool = true
    /// Allows user to zoom the sticker by pinching on the sticker view. Defaults to true.
    var allowPinchToZoom : Bool = false
    /// Allows the user to rotate the sticker by using 2 finger rotation on the view. Defaults to true.
    var allowRotationGesture : Bool = false
    /// Allows the user drag the sticker view around.
    var allowDragging : Bool = true
    /// Defines the color of the border drawn around the content view. Defaults to gray.
    var borderColor : UIColor = UIColor.gray
    /// Defines the width of the border drawn around the sticker view.
    var borderWidth : CGFloat = 1.0
    var minWidth : CGFloat?
    var minHeight : CGFloat?

    var borderView : SPGripViewBorderView?

    var resizingControl : UIImageView?
    var deleteControl : UIImageView?

    var pinchRecognizer : UIPinchGestureRecognizer?
    var rotationRecognizer : UIRotationGestureRecognizer?

    var preventsLayoutWhileResizing : Bool?

    var deltaAngle : CGFloat?
    var prevPoint : CGPoint?
    var startTransform : CGAffineTransform?

    var touchStart : CGPoint?

    
    var parentFrame : CGRect = .zero
    
    
    static func == (lhs: JRStickerView, rhs: JRStickerView) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    let identifier = UUID()
    //MARK: initial

    
    @objc private func doubleClickHandler (sender : UITapGestureRecognizer){
        if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.textStickerDoubleClick(textSticker:))) {
            stickerViewDelegate!.textStickerDoubleClick?(textSticker: self)
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupDefaultAttributes()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setFrame(_ newFrame: CGRect) {
        super.frame = newFrame
        contentView!.frame = bounds.insetBy(dx: kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize / 2, dy: kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize / 2)

        contentView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        for subview in contentView!.subviews {
            subview.frame = CGRect(x: 0, y: 0, width: contentView!.frame.size.width, height: contentView!.frame.size.height)

            subview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }

        borderView!.frame = bounds.insetBy(dx: CGFloat(kSPUserResizableViewGlobalInset), dy: CGFloat(kSPUserResizableViewGlobalInset))

        resizingControl!.frame = CGRect(x: bounds.size.width - kZDStickerViewControlSize, y: bounds.size.height - kZDStickerViewControlSize, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize)

        deleteControl!.frame = CGRect(x: 0, y: 0, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize)



        borderView!.setNeedsDisplay()
    }

    
    
    func setContentView(_ newContentView: UIView?) {
        //contentView!.removeFromSuperview()
        contentView = newContentView
        contentView?.isUserInteractionEnabled = true
        contentView?.backgroundColor = UIColor(named: "zdStickerBackground") ?? UIColor.clear
        contentView!.frame = bounds.insetBy(dx: CGFloat(kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize / 2), dy: kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize / 2)

        self.contentView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        self.addSubview(contentView!)

        for subview in contentView!.subviews {
            subview.frame = CGRect(x: 0, y: 0, width: contentView!.frame.size.width, height: contentView!.frame.size.height)

            subview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }

        bringSubviewToFront(borderView!)
        bringSubviewToFront(resizingControl!)
        bringSubviewToFront(deleteControl!)

    }

    func setupDefaultAttributes()  {
        borderView = SPGripViewBorderView(frame: bounds.insetBy(dx: CGFloat(kSPUserResizableViewGlobalInset), dy: CGFloat(kSPUserResizableViewGlobalInset)))
        borderView!.borderColor = borderColor
        borderView!.borderWidth = borderWidth
        borderView!.isHidden = false
        addSubview(borderView!)

        if kSPUserResizableViewDefaultMinWidth > bounds.size.width * 0.5 {
            minWidth = kSPUserResizableViewDefaultMinWidth
            minHeight = bounds.size.height * (kSPUserResizableViewDefaultMinWidth / bounds.size.width)
        } else {
            minWidth = bounds.size.width * 0.5
            minHeight = bounds.size.height * 0.5
        }
        self.preventsPositionOutsideSuperview = true;
        self.preventsLayoutWhileResizing = true;
        self.preventsResizing = false;
        self.preventsDeleting = false;
        self.preventsCustomButton = true;
        self.translucencySticker = true;
        self.allowDragging = true;
        self.isUserInteractionEnabled = true
        
        #if ZDSTICKERVIEW_LONGPRESS
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        addGestureRecognizer(longpress)
        #endif

        deleteControl = UIImageView(frame: CGRect(x: 0, y: 0, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize))
        deleteControl!.backgroundColor = UIColor.clear
        deleteControl!.image = UIImage(named: "ZDStickerView.bundle/ZDBtn3.png")
        deleteControl!.isUserInteractionEnabled = true
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(singleTap(_:)))
        deleteControl!.addGestureRecognizer(singleTap)
        addSubview(deleteControl!)

        resizingControl = UIImageView(frame: CGRect(x: frame.size.width - kZDStickerViewControlSize, y: frame.size.height - kZDStickerViewControlSize, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize))
        resizingControl!.backgroundColor = UIColor.clear
        resizingControl!.isUserInteractionEnabled = true
        resizingControl!.image = UIImage(named: "ZDStickerView.bundle/ZDBtn2.png.png")
        let panResizeGesture = UIPanGestureRecognizer(target: self, action: #selector(resizeTranslate(_:)))
        resizingControl!.addGestureRecognizer(panResizeGesture)
        addSubview(resizingControl!)


        // Add pinch gesture recognizer.
        pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinchTranslate(_:)))
        //addGestureRecognizer(pinchRecognizer!)

        // Add rotation recognizer.
        rotationRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(rotateTranslate(_:)))
        //addGestureRecognizer(rotationRecognizer!)



        deltaAngle = atan2(frame.origin.y + frame.size.height - center.y, frame.origin.x + frame.size.width - center.x)


    }
    
    //MARK: touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !allowDragging {
            return
        }

        enableTransluceny(state: true)

        let touch = touches.first
        touchStart = touch?.location(in: superview)
        if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidBeginEditing(sticker:))) {
            stickerViewDelegate!.stickerViewDidBeginEditing!(sticker: self)
            
        }
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        enableTransluceny(state: false)

        // Notify the delegate we've ended our editing session.
        if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidEndEditing(sticker:))) {
            stickerViewDelegate!.stickerViewDidEndEditing!(sticker: self)
            
        }
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        enableTransluceny(state: false)

        // Notify the delegate we've ended our editing session.
        if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidCancelEditing(sticker:))) {
            stickerViewDelegate!.stickerViewDidCancelEditing!(sticker: self)
            
        }
    }
    func translate(usingTouchLocation touchPoint: CGPoint) {
        var newCenter = CGPoint(x: center.x + touchPoint.x - touchStart!.x, y: center.y + touchPoint.y - touchStart!.y)

        if preventsPositionOutsideSuperview {
            // Ensure the translation won't cause the view to move offscreen.
            let midPointX = bounds.midX
            if newCenter.x > superview!.bounds.size.width - midPointX {
                newCenter.x = superview!.bounds.size.width - midPointX
            }

            if newCenter.x < midPointX {
                newCenter.x = midPointX
            }

            let midPointY = bounds.midY
            if newCenter.y > superview!.bounds.size.height - midPointY {
                newCenter.y = superview!.bounds.size.height - midPointY
            }

            if newCenter.y < midPointY {
                newCenter.y = midPointY
            }
        }

        center = newCenter
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !allowDragging{
            return
        }
        self.enableTransluceny(state: true)
        let touchLocation = touches.first?.location(in: self)
        if resizingControl!.frame.contains(touchLocation!){
            return
        }
        let touch = touches.first?.location(in: self.superview)
        if touchStart != nil{
            translate(usingTouchLocation: touch!)
        }
      
        touchStart = touch
    }
    
    //MARK: long pressed
    #if ZDSTICKERVIEW_LONGPRESS
    func longPress(_ recognizer: UIPanGestureRecognizer?) {
        if recognizer?.state == .began {
            if stickerViewDelegate.responds(to: #selector(stickerViewDidLongPressed(_:))) {
                stickerViewDelegate.stickerViewDidLongPressed(self)
            }
        }
    }
    #endif
  //MARK: gesture
    //  Converted to Swift 5.1 by Swiftify v5.1.28520 - https://objectivec2swift.com/
    @objc func singleTap(_ recognizer: UIPanGestureRecognizer?) {
        
        if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidClose(sticker:))) {
            stickerViewDelegate!.stickerViewDidClose!(sticker: self)
//            do{
//                try FileManager.default.removeItem(atPath: path!)
//                print("path remove success")
//            }catch{
//                print(error)
//            }
        }

        if false == preventsDeleting {
            let close = recognizer?.view
            close?.superview?.removeFromSuperview()
        }
    }


    func customTap(_ recognizer: UIPanGestureRecognizer?) {
        if false == preventsCustomButton {
            if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidCustomButtonTap(sticker:))) {
                stickerViewDelegate!.stickerViewDidCustomButtonTap!(sticker: self)
            }
        }
    }
   

    @objc func pinchTranslate(_ recognizer: UIPinchGestureRecognizer?) {
        var boundsBeforeScaling = CGRect.zero
        var transformBeforeScaling: CGAffineTransform? = CGAffineTransform.identity
        if recognizer?.state == .began {
            boundsBeforeScaling = recognizer?.view?.bounds ?? CGRect.zero
            transformBeforeScaling = recognizer?.view?.transform
        }

        let center = recognizer?.view?.center
        let scale : CGAffineTransform = CGAffineTransform.init(scaleX: recognizer!.scale, y: recognizer!.scale)
        //let scale: CGAffineTransform = .identity.scaledBy(x: recognizer?.scale, y: recognizer?.scale)
        var frame = boundsBeforeScaling.applying(scale)

        frame.origin = CGPoint(x: (center?.x ?? 0.0) - frame.size.width / 2, y: (center?.y ?? 0.0) - frame.size.height / 2)

        recognizer?.view?.transform = .identity
        recognizer?.view?.frame = frame
        recognizer?.view?.transform = transformBeforeScaling!
    }
    @objc func rotateTranslate(_ recognizer: UIRotationGestureRecognizer?) {
        recognizer?.view?.transform = (recognizer?.view?.transform.rotated(by: recognizer!.rotation))!
        recognizer?.rotation = 0
        
    }
    
    @objc func resizeTranslate(_ recognizer: UIPanGestureRecognizer?) {
        if recognizer?.state == .began {
            enableTransluceny(state: true)
            prevPoint = (recognizer?.location(in: self))!
            
            
            
            setNeedsDisplay()
            // Inform delegate.
            if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidBeginEditing(sticker:))) {
                stickerViewDelegate!.stickerViewDidBeginEditing!(sticker: self)
            }
        } else if recognizer?.state == .changed {
            enableTransluceny(state: true)
            
            // preventing from the picture being shrinked too far by resizing
            if Float(bounds.size.width) < Float(minWidth!) || Float(bounds.size.height) < Float(minHeight!) {
                bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: minWidth! + 1, height: minHeight! + 1)
                resizingControl!.frame = CGRect(x: bounds.size.width - kZDStickerViewControlSize, y: bounds.size.height - kZDStickerViewControlSize, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize)
                deleteControl!.frame = CGRect(x: 0, y: 0, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize)

                prevPoint = (recognizer?.location(in: self))!
            }
            //Resizing
            else{
                
                let point = (recognizer?.location(in: self))!
                var wChange: CGFloat = 0.0
                var hChange: CGFloat = 0.0

                wChange = (point.x - prevPoint!.x)
                let wRatioChange = wChange / bounds.size.width

                hChange = wRatioChange * bounds.size.height

                if abs(wChange) > 50.0 || abs(hChange) > 50.0 {
                    prevPoint = (recognizer?.location(ofTouch: 0, in: self))!
                    return
                }

                bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: CGFloat(bounds.size.width + wChange), height: CGFloat(bounds.size.height + hChange))
                resizingControl!.frame = CGRect(x: bounds.size.width - kZDStickerViewControlSize, y: bounds.size.height - kZDStickerViewControlSize, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize)
                deleteControl!.frame = CGRect(x: 0, y: 0, width: kZDStickerViewControlSize, height: kZDStickerViewControlSize)


                prevPoint = (recognizer?.location(ofTouch: 0, in: self))!
            }
            
            // Rotation
            let ang = atan2(recognizer!.location(in: superview).y - center.y, recognizer!.location(in: superview).x - center.x)

            let angleDiff = deltaAngle! - ang

            if false == preventsResizing {
                transform = CGAffineTransform(rotationAngle: -angleDiff)
            }

            borderView!.frame = bounds.insetBy(dx: CGFloat(kSPUserResizableViewGlobalInset), dy: kSPUserResizableViewGlobalInset)
            borderView!.setNeedsDisplay()

            setNeedsDisplay()
        }else if recognizer!.state == .ended {
            enableTransluceny(state: false)
            prevPoint = (recognizer?.location(in: self))!
            setNeedsDisplay()
            
            // Inform delegate.
            if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidEndEditing(sticker:))) {
                stickerViewDelegate!.stickerViewDidEndEditing!(sticker: self)
            }
            
            
            
            
        } else if recognizer!.state == .cancelled {
            
           
            // Inform delegate.
            if stickerViewDelegate!.responds(to: #selector(stickerViewDelegate?.stickerViewDidCancelEditing(sticker:))) {
                stickerViewDelegate!.stickerViewDidCancelEditing!(sticker: self)
            }
        }

        
        
    }
    
    //MARK: public method
    func hideDelHandle() {
        deleteControl?.isHidden = true
    }
    func showDelHandle() {
        deleteControl?.isHidden = false
    }
    func hideEditingHandles()  {
        contentView?.backgroundColor = .clear
        self.allowPinchToZoom = false;
        self.allowRotationGesture = false;
        
        self.resizingControl!.isHidden = true;
        self.deleteControl!.isHidden = true;
        
        self.borderView?.isHidden = true
    }
    func showEditingHandles()  {
        contentView?.backgroundColor = UIColor(named: "zdStickerBackground") ?? UIColor.clear
        if (false == self.preventsDeleting)
        {
            self.deleteControl!.isHidden = false;
        }
        else
        {
            self.deleteControl!.isHidden = true;
        }
        
        if (false == self.preventsResizing)
        {
            self.resizingControl!.isHidden = false;
        }
        else
        {
            self.resizingControl!.isHidden = true;
        }
        
        //extra add
        //self.allowDragging = YES;
        self.allowPinchToZoom = true;
        self.allowRotationGesture = true;
        
        self.borderView?.isHidden = false
    }
    
    func setButton(type:ZDStickerViewButton, image : UIImage) {
        switch type {
        case .ZDStickerViewButtonResize:
            self.resizingControl?.image = image
            break
        case .ZDStickerViewButtonDel:
            self.deleteControl?.image = image
            break
        default:
            break
        }
    }
    func isEditingHandlesHidden() -> Bool {
        return self.borderView!.isHidden
    }
    
    func enableTransluceny(state: Bool) {
        if translucencySticker == true{
            if state == true{
                self.alpha = 0.65
            }else {
                self.alpha = 1.0
            }
        }
    }
    func getBorderColor() -> UIColor {
        return self.borderView!.borderColor
    }
    func setBorderColor(borderColor : UIColor) {
        self.borderView!.borderColor = borderColor;
    }
    func getBorderWidth() -> CGFloat {
        return self.borderView!.borderWidth
    }
    func setBorderWidth(borderWidth : CGFloat) {
        self.borderView!.borderWidth = borderWidth
    }
    func getAllowPinchToZoom() -> Bool {
        return self.pinchRecognizer!.isEnabled
    }
    func setAllowPinchToZoom(allowPinchToZoom : Bool){
        self.pinchRecognizer!.isEnabled = allowPinchToZoom
    }
    func getAllowRotationGesture() -> Bool{
        return self.rotationRecognizer!.isEnabled
    }
    func setAllowRotationGesture(allowRotationGesture : Bool) {
        self.rotationRecognizer!.isEnabled = allowRotationGesture
    }
    

    func getChildFrame() -> CGRect {
        let rect = convert(contentView!.frame, to: superview)
        return rect
    }
    
    
}
