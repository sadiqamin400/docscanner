//
//  EditScanVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 1/6/21.
//

import UIKit
import AVFoundation

protocol CropDelegate: NSObjectProtocol {
    func onCropDone(url : URL)
    func onCropError()
}

class EditScanVC: UIViewController {
    weak var delegate : CropDelegate?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRetake: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    private lazy var quadView: QuadrilateralView = {
        let quadView = QuadrilateralView()
        quadView.editable = true
        quadView.translatesAutoresizingMaskIntoConstraints = false
        return quadView
    }()
    
    var indexValue = 0
    /// The image the quadrilateral was detected on.
    private let image: UIImage
    
    /// The detected quadrilateral that can be edited by the user. Uses the image's coordinates.
    private var quad: Quadrilateral
    
    private var zoomGestureController: ZoomGestureController!
    
    private var quadViewWidthConstraint = NSLayoutConstraint()
    private var quadViewHeightConstraint = NSLayoutConstraint()
    
    // MARK: - Life Cycle
    
    init(image: UIImage, quad: Quadrilateral?, rotateImage: Bool = true) {
        self.image = rotateImage ? image.applyingPortraitOrientation() : image
        self.quad = quad ?? EditScanVC.defaultQuad(forImage: image)
        super.init(nibName: "EditScanVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupConstraints()
        
        imageView.image = image
        
        zoomGestureController = ZoomGestureController(image: image, quadView: quadView)
        
        let touchDown = UILongPressGestureRecognizer(target: zoomGestureController, action: #selector(zoomGestureController.handle(pan:)))
        touchDown.minimumPressDuration = 0
        contentView.addGestureRecognizer(touchDown)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        adjustQuadViewConstraints()
        displayQuad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Work around for an iOS 11.2 bug where UIBarButtonItems don't get back to their normal state after being pressed.
    }
    
    // MARK: - Setups
    
    private func setupViews() {
        
        contentView.addSubview(quadView)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Store.sharedInstance.isAlreadyScanPage = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        Store.sharedInstance.isAlreadyScanPage = false
        
    }
    private func setupConstraints() {

        quadViewWidthConstraint = quadView.widthAnchor.constraint(equalToConstant: 0.0)
        quadViewHeightConstraint = quadView.heightAnchor.constraint(equalToConstant: 0.0)
        
        let quadViewConstraints = [
            quadView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            quadView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            quadViewWidthConstraint,
            quadViewHeightConstraint
        ]
        
        NSLayoutConstraint.activate(quadViewConstraints )
    }
    
    @IBAction func onCancelPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
//        if let imageScannerController = navigationController as? ImageScannerController {
//            imageScannerController.imageScannerDelegate?.imageScannerControllerDidCancel(imageScannerController)
//        }
    }
    
    // MARK: - Actions
    
    func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
           let oldWidth = sourceImage.size.width
           let scaleFactor = scaledToWidth / oldWidth
           
           let newHeight = sourceImage.size.height * scaleFactor
           let newWidth = oldWidth * scaleFactor
           
           UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
           sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
           let newImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           return newImage!
    }
       
    
    @IBAction func onDonePressed(_ sender: Any) {
        pushReviewController()
    }
    
    @objc func pushReviewController() {
       
        let fileName = "\(Int(Date().timeIntervalSince1970 * 1000))" + ".jpg"
        
        guard let quad = quadView.quad,
            let ciImage = CIImage(image: image) else {
            dismiss(animated: true) { [self] in
                if let delegate = delegate{
                    delegate.onCropError()
                }
            }
                return
        }
        let cgOrientation = CGImagePropertyOrientation(image.imageOrientation)
        let orientedImage = ciImage.oriented(forExifOrientation: Int32(cgOrientation.rawValue))
        let scaledQuad = quad.scale(quadView.bounds.size, image.size)
        self.quad = scaledQuad
        
        // Cropped Image
        var cartesianScaledQuad = scaledQuad.toCartesian(withHeight: image.size.height)
        cartesianScaledQuad.reorganize()
        
        let filteredImage = orientedImage.applyingFilter("CIPerspectiveCorrection", parameters: [
            "inputTopLeft": CIVector(cgPoint: cartesianScaledQuad.bottomLeft),
            "inputTopRight": CIVector(cgPoint: cartesianScaledQuad.bottomRight),
            "inputBottomLeft": CIVector(cgPoint: cartesianScaledQuad.topLeft),
            "inputBottomRight": CIVector(cgPoint: cartesianScaledQuad.topRight)
            ])
        
        let croppedImage = UIImage.from(ciImage: filteredImage)
        // Enhanced Image
        let enhancedImage = filteredImage.applyingAdaptiveThreshold()?.withFixedOrientation()
        let enhancedScan = enhancedImage.flatMap { ImageScannerScan(image: $0) }
        
        let results = ImageScannerResults(detectedRectangle: scaledQuad, originalScan: ImageScannerScan(image: image), croppedScan: ImageScannerScan(image: croppedImage), enhancedScan: enhancedScan)
       
       var convertedImage =  imageWithImage(sourceImage: results.croppedScan.image , scaledToWidth: 1500)
        convertedImage = convertedImage.resizeWithSize(targetSize: CGSize(width: Constant.a4Width, height: Constant.a4Height))
        
       
           let fileManager = FileManager.default
           do {
               let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
               let fileURL = documentDirectory.appendingPathComponent(fileName)
               if let imageData =  convertedImage.jpegData(compressionQuality:  0.5) {
                   try imageData.write(to: fileURL)
                if let delegate = delegate{
                    self.dismiss(animated: true) {
                        delegate.onCropDone(url: fileURL)
                    }
                }
          
               }
           } catch {
               print(error)
            if let delegate = delegate{
                self.dismiss(animated: true) {
                    delegate.onCropError()
                }
            }
        }
           
   }
       
        
       
    

    private func displayQuad() {
        let imageSize = image.size
        let imageFrame = CGRect(origin: quadView.frame.origin, size: CGSize(width: quadViewWidthConstraint.constant, height: quadViewHeightConstraint.constant))
        
        let scaleTransform = CGAffineTransform.scaleTransform(forSize: imageSize, aspectFillInSize: imageFrame.size)
        let transforms = [scaleTransform]
        let transformedQuad = quad.applyTransforms(transforms)
        
        quadView.drawQuadrilateral(quad: transformedQuad, animated: false)
    }
    
    /// The quadView should be lined up on top of the actual image displayed by the imageView.
    /// Since there is no way to know the size of that image before run time, we adjust the constraints to make sure that the quadView is on top of the displayed image.
    private func adjustQuadViewConstraints() {
        let frame = AVMakeRect(aspectRatio: image.size, insideRect: imageView.bounds)
        quadViewWidthConstraint.constant = frame.size.width
        quadViewHeightConstraint.constant = frame.size.height
    }
    
    /// Generates a `Quadrilateral` object that's centered and one third of the size of the passed in image.
    private static func defaultQuad(forImage image: UIImage) -> Quadrilateral {
        let topLeft = CGPoint(x: image.size.width / 3.0, y: image.size.height / 3.0)
        let topRight = CGPoint(x: 2.0 * image.size.width / 3.0, y: image.size.height / 3.0)
        let bottomRight = CGPoint(x: 2.0 * image.size.width / 3.0, y: 2.0 * image.size.height / 3.0)
        let bottomLeft = CGPoint(x: image.size.width / 3.0, y: 2.0 * image.size.height / 3.0)
        
        let quad = Quadrilateral(topLeft: topLeft, topRight: topRight, bottomRight: bottomRight, bottomLeft: bottomLeft)
        
        return quad
    }

}

