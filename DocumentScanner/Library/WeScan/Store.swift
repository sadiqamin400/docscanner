//
//  Store.swift
//  FinalDocumentScanner
//
//  Created by MacBook Pro Retina on 19/10/19.
//  Copyright © 2019 MacBook Pro Retina. All rights reserved.
//

import UIKit
import StoreKit
import Photos
import PhotosUI
class Store: NSObject {
    
    var storeArray:NSMutableArray!
    var fontArray:NSArray!
    var fontName = "sadiq"
    var products: [SKProduct] = []
    var imageArray:[String] = []
    var shouldPop:Bool!
    var shouldPresentDatabse = false
    var isNeededToUpdate = true
    //// var fArray: [FolderHolder] = []
    var iscalledPurchase = false
    var isPurchase = false
    var needTopresent = false
    var currentItem:String!
    var isfromFastIndex = 0
    var isMessedShowen = 0
    var isSubscriptionActive  =  false
    var tabarView:UITabBarController!
    var needToShow = false
    var showImage = false
    var selectedImage:UIImage!
    var shouldShowScanner = false
    var shouldShowHomeScreen = false
    var lastImage:UIImage?
    var currentSection:String?
    var currentFileName:String?
    var buttonPressed = 1
    var isAlreadyScanPage = false
    var isUpdatedF = false
    var currentNameFile = ""
    var doneBtnPressed = false
    var termsOfUseValue = "https://sites.google.com/view/zert-interactive/terms-of-service?authuser=0"
    var privacyPolicyValue = "https://sites.google.com/view/zert-interactive/privacy-policy?authuser=0"
    var manageSubsciptions = "https://sites.google.com/view/zert-interactive/others/manage-subscriptions"
    var troubleshoot = "https://sites.google.com/view/zert-interactive/others/troubleshoot"
    
    static let sharedInstance = Store()
    var tempoRaryfile = 0
    var counterTempFile = 0
    
    
    
   
    
    fileprivate override init() {
        
        
        storeArray = NSMutableArray()
        shouldPop = false
       
        //This prevents others from using the default '()' initializer
        super.init()
    }
    func settabarView(tabBar:UITabBarController){
        tabarView = tabBar
    }
    func getTabrBar()->UITabBarController{
        return tabarView
    }
    func getsshouldShowHomeScreen()->Bool
    {
        return shouldShowHomeScreen
    }
    func setshouldShowHomeScreen(value:Bool)
    {
        shouldShowHomeScreen = value
    }
    func setshouldShowScanner(value:Bool)
    {
        shouldShowScanner = value
    }
    func getshouldShowScanner()->Bool
    {
        return shouldShowScanner
    }
    func setArray(array: NSMutableArray)
    {
        storeArray = array
    }
    func setIsCalledPurchase(value: Bool)
    {
        iscalledPurchase = value
    }
    func setfromFastIndex(value:Int)
    {
        isfromFastIndex = value
    }
    func getfromFastIndex () ->Int
    {
        return isfromFastIndex
    }
    func needToShowValue(value:Bool)
    {
        needToShow = value
    }
    func getneedToShow () ->Bool
    {
        return needToShow
    }
    func setisMessedShowen(value:Int)
    {
        isMessedShowen = value
    }
    func getisMessedShowen () ->Int
    {
        return isMessedShowen
    }
    func setCurrentItem(value: String)
    {
        currentItem = value
    }
    func getCurrentItem() -> String
    {
        return currentItem
        
    }
    func getsCalledPurchase()->Bool
    {
        return iscalledPurchase
    }
    func seturchase(value: Bool)
    {
        isPurchase = value
    }
    func getPurchase()->Bool
    {
        return isPurchase
    }
    
    func setPurchaseActive(value: Bool)
    {
        isSubscriptionActive = value
    }
    func getIsPurchaseActive()->Bool
    {
        return isSubscriptionActive
    }
    func setneedTopresent(value: Bool)
    {
        needTopresent = value
    }
    func getneedTopresent()->Bool
    {
        return needTopresent
    }
    func getArray() -> NSMutableArray {
        return storeArray
    }
    
    func setfontArray(array: NSArray)
    {
        fontArray = array
    }
    func getfontArray() -> NSArray {
        return fontArray
    }
    func setFontName(name: NSString)
    {
        fontName = name as String
    }
    func getFontName() -> NSString {
        return fontName as NSString
    }
    func setProductArray(array:NSArray)
    {
        products = array as! [SKProduct]
    }
    func getProductArray() -> NSArray {
        return products as NSArray
    }
    func setImageArray(array:[String])
    {
        imageArray = array
    }
    func getImageArray() -> [String] {
        return imageArray  
    }
    func setPopValue(value:Bool)
    {
        shouldPop = value
    }
    func getPopValue()->Bool
    {
        return shouldPop
    }
    func getIsNeededToBeUpdated()->Bool
    {
        return isNeededToUpdate
    }
    
    func setDatabaseValue(value:Bool)
    {
        shouldPresentDatabse = value
    }
    
    func setUpdateTableValue(value:Bool)
    {
        isNeededToUpdate = value
    }
    func getDatabaseValue()->Bool
    {
        return shouldPresentDatabse
    }
    //    func setFolderArray(_ arr: [FolderHolder])
    //    {
    //        fArray = arr
    //    }
    //    func getFolderArray() -> [FolderHolder] {
    //        return  fArray
    //    }
    
    func setDate(date:NSDate)
    {
        let returnValue: [NSString]? = UserDefaults.standard.object(forKey: "PURCHASED_EXPIRE_DATE") as? [NSString]
        
        if(returnValue?.count == 0)
        {
            UserDefaults.standard.set(date, forKey: "PURCHASED_EXPIRE_DATE")
        }
        
    }
    
    
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
                return formatter.date(from: expiresDate)
            }
            
            return nil
        }
        else {
            return nil
        }
    }
    func setDate(date:Date?)
    {
        if let valueForDate = date
        {
            UserDefaults.standard.set(valueForDate, forKey: "PURCHASED_EXPIRE_DATE")
        }
    }
    
}
