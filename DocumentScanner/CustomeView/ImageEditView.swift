//
//  ImageEditView.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 8/6/21.
//

import UIKit

class ImageEditView: UIView {

    private var imageView : UIImageView = UIImageView()
    private var model : ImageModel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func commonInit(){
        imageView.frame = self.bounds
        imageView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        addSubview(imageView)
        imageView.clipsToBounds = true
        
        self.clipsToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.6
    }

    func setModel(model : ImageModel){
        self.model = model
        let data = try! Data(contentsOf: model.image)
        //need to apply all settings
        self.imageView.image = UIImage(data: data)
    }
    
}
