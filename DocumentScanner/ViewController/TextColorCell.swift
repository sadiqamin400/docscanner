//
//  TextColorCell.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 7/6/21.
//

import UIKit

class TextColorCell: UICollectionViewCell {

    static let identifire = "TextColorCell"
    
    @IBOutlet weak var ivImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clipsToBounds = true
        ivImage.clipsToBounds = true
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.white.cgColor
    }
    
    override func prepareForReuse() {
        ivImage.image = nil
        self.layer.cornerRadius = self.frame.height / 2
        self.ivImage.layer.cornerRadius = self.ivImage.frame.height / 2
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.height / 2
        self.ivImage.layer.cornerRadius = self.ivImage.frame.height / 2
    }

    override var isSelected: Bool{
        didSet{
            if isSelected{
                self.layer.borderWidth = 2
            }else{
                self.layer.borderWidth = 0
            }
        }
    }
    static func nib()-> UINib{
        return UINib(nibName: identifire, bundle: nil)
    }
}
