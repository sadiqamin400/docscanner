//
//  TextEditView.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 6/6/21.
//

import UIKit

class TextEditView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblTextColor: UILabel!
    @IBOutlet weak var lblFillColor: UILabel!
    @IBOutlet weak var lblFont: UILabel!
    @IBOutlet weak var lblFontSize: UILabel!
    @IBOutlet weak var lblAlignment: UILabel!
    @IBOutlet weak var lblFontSizeCount: UILabel!
    @IBOutlet weak var stepperFontSize: UIStepper!
    
    @IBOutlet weak var btnLeftAlign: UIButton!
    @IBOutlet weak var btnCenterAlign: UIButton!
    @IBOutlet weak var btnRightAlign: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var collectionTextColor: UICollectionView!
    @IBOutlet weak var collectionFillColor: UICollectionView!
    @IBOutlet weak var topView: UIView!
    
    private var textColors : [UIColor] = [UIColor.black,UIColor.red,UIColor.blue,UIColor.green,UIColor.white]
    private var fillColors : [UIColor] = [UIColor.white,UIColor.black,UIColor.orange,UIColor.green]
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("TextEditView", owner: self, options:[:])
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentView)
        
        //localize
        lblFont.text = NSLocalizedString("Font", comment: "")
        lblFillColor.text = NSLocalizedString("FillColor", comment: "")
        lblFont.text = NSLocalizedString("Font", comment: "")
        lblFontSize.text = NSLocalizedString("Size", comment: "")
        lblAlignment.text = NSLocalizedString("Alignment", comment: "")
        
        collectionTextColor.register(TextColorCell.nib(), forCellWithReuseIdentifier: TextColorCell.identifire)
        collectionFillColor.register(TextColorCell.nib(), forCellWithReuseIdentifier: TextColorCell.identifire)
        
        if let layout = collectionFillColor.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.scrollDirection = .horizontal
            
        }
        if let layout = collectionTextColor.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.scrollDirection = .horizontal
            
        }
        collectionFillColor.contentInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        collectionTextColor.contentInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        
        
        topView.layer.cornerRadius = 5
        topView.layer.shadowColor = UIColor.gray.cgColor
        topView.layer.shadowOffset = .zero
        topView.layer.shadowRadius = 1
        topView.layer.shadowOpacity = 0.6
    }

    func update(){
        let w = collectionTextColor.bounds.width - collectionTextColor.contentSize.width
        collectionTextColor.contentOffset = CGPoint(x: -w, y: collectionTextColor.contentOffset.y)
        let x = collectionFillColor.bounds.width - collectionFillColor.contentSize.width
        collectionFillColor.contentOffset = CGPoint(x: -x, y: collectionFillColor.contentOffset.y)
    }
    
}

extension TextEditView : UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionTextColor{
            return textColors.count
        }
        return fillColors.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TextColorCell.identifire, for: indexPath) as? TextColorCell else {
            fatalError("cell load error")
        }
        
        if collectionView == collectionTextColor{
            cell.ivImage.backgroundColor = textColors[indexPath.row]
            return cell
        }
        
        if indexPath.row == 0{
            cell.ivImage.image = UIImage(named: "none")
            cell.ivImage.backgroundColor = UIColor.white
            return cell
        }
        cell.ivImage.backgroundColor = fillColors[indexPath.row-1]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = collectionView.bounds.height - 8
        return CGSize(width: h, height: h)
    }
    
}
