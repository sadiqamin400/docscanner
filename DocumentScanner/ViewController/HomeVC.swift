//
//  HomeVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 27/5/21.
//

import UIKit

class HomeVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivPro: UIImageView!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnViewMarge: HomeItem!
    @IBOutlet weak var btnSortMove: HomeItem!
    @IBOutlet weak var btnFolderDelete: HomeItem!
    @IBOutlet weak var btnSettingShare: HomeItem!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localize()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onSelectPressed(_ sender: Any) {
        if (btnSelect.titleLabel!.text!.elementsEqual(NSLocalizedString("Select", comment: ""))){
            btnSelect.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
            self.isSelect(mode: true)
        }else{
            btnSelect.setTitle(NSLocalizedString("Select", comment: ""), for: .normal)
            self.isSelect(mode: false)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onScanPressed(_ sender: Any) {
        let scan = ScanVC(nibName: "ScanVC", bundle: nil)
//        scan.modalPresentationStyle = .fullScreen
//        self.present(scan, animated: true) {
//
//        }
        self.navigationController?.pushViewController(scan, animated: true)
    }
    
}


//MARK:-- menu actons delegate
extension HomeVC : HomeItemDelegate{
    func onSelected(sender: HomeItem) {
        switch sender {
        case btnViewMarge:
            margeAlert {
                
            }
            break
        case btnSortMove:
            
            break
        case btnFolderDelete:
            deleteAlert {
                
            }
            break
        case btnSettingShare:
            shareAlert()
            break
        default:
            break
        }
    }
    
    func onDeselected(sender: HomeItem) {
        switch sender {
        case btnViewMarge:
            
            break
        case btnSortMove:
            sortAlert()
            break
        case btnFolderDelete:
            createFolderAlert { folderName in
                print(folderName)
            }
            break
        case btnSettingShare:
            shareAlert()
            break
        default:
            break
        }
    }
    
    
}
