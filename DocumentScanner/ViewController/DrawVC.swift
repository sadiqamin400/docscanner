//
//  DrawVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 4/6/21.
//

import UIKit

class DrawVC: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnRedo: UIButton!
    @IBOutlet weak var btnUndo: UIButton!
    @IBOutlet weak var stepper: UIStepper!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onBackPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    
}
