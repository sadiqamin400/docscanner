//
//  DocEditVC+Extension.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit

extension DocEditVC : UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return editList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EditItemCell.identifire, for: indexPath) as? EditItemCell else {
            fatalError("Cell load failed")
        }
        
        let obj = editList[indexPath.row]
        cell.setData(model: obj)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = collectionView.bounds.height - 16
        return CGSize(width: h, height: h)
    }
    
}
extension DocEditVC {
    func collectionViewSetup() {
        collectionView.register(EditItemCell.nib(), forCellWithReuseIdentifier: EditItemCell.identifire)
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.scrollDirection = .horizontal
        }
    }
    func localize()  {
        btnDone.setTitle(NSLocalizedString("Done", comment: ""), for: .normal)
        segment.setTitle(NSLocalizedString("Image", comment: ""), forSegmentAt: 0)
        segment.setTitle(NSLocalizedString("Text_Detect", comment: ""), forSegmentAt: 1)
        
        lblPages.layer.cornerRadius = 20
    }
    
}
