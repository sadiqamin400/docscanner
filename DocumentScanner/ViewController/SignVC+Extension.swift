//
//  SignVC+Extension.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit
extension SignVC{
    func addSegmentController(){
        let titles = [NSLocalizedString("Sign", comment: ""), NSLocalizedString("All_Sign", comment: "")]
        segmentControl = UISegmentedControl(items: titles)
        //egmentControl!.tintColor = UIColor.white
        
        segmentControl!.selectedSegmentIndex = 0
//        for index in 0...titles.count-1 {
//            segmentControl!.setWidth(120, forSegmentAt: index)
//        }
        segmentControl!.sizeToFit()
        segmentControl!.addTarget(self, action: #selector(onSegmentChange(sender:)), for: .valueChanged)
        segmentControl!.selectedSegmentIndex = 0
        segmentControl!.sendActions(for: .valueChanged)
        navigationItem.titleView = segmentControl
    }
    func showHideRightBarButton(isHide : Bool) {
        if !isHide {
            self.navigationItem.rightBarButtonItem  = rightButton
        } else {
            self.navigationItem.setRightBarButton(nil, animated: true)
        }
    }
    
}
