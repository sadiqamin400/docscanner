//
//  SignantureCell.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 3/6/21.
//

import UIKit

class SignatureCell: UITableViewCell {

    static let identifire = "SignatureCell"
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var ivImage: UIImageView!
    
    private var shadowLayer: CAShapeLayer = CAShapeLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        //setShadow()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        contentView.layer.cornerRadius = 10
        contentView.layer.shadowColor = UIColor.gray.cgColor
        contentView.layer.shadowOffset = .zero
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowOpacity = 0.6
        contentView.layer.shouldRasterize = true
        contentView.layer.rasterizationScale = UIScreen.main.scale
    }
    
    override func prepareForReuse() {
        //ivImage.image = nil
    }
    
    private func setShadow(){
        shadowLayer.path = UIBezierPath(roundedRect: shadowView.bounds, cornerRadius: 10).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor
        shadowLayer.cornerRadius = 10
        shadowLayer.shadowColor = UIColor.gray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = .zero //CGSize(width: 2.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.6
        shadowLayer.shadowRadius = 1
        shadowLayer.rasterizationScale = UIScreen.main.scale
        shadowLayer.shouldRasterize = true
        
        
        shadowLayer.removeFromSuperlayer()
        shadowView.layer.insertSublayer(shadowLayer, at: 0)
    }
    
    static func nib()-> UINib{
        return UINib(nibName: identifire, bundle:nil)
    }
}
