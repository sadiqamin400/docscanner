//
//  HomeVC+Extension.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 29/5/21.
//

import UIKit

extension HomeVC{
    
    func localize(){
        lblTitle.text = NSLocalizedString("Home_Title", comment: "")
        btnSelect.setTitle(NSLocalizedString("Select", comment: ""), for: .normal)
        btnSelect.setTitle(NSLocalizedString("Cancel", comment:""), for: .selected)
        
        btnViewMarge.setSelectValue(title: NSLocalizedString("Marge", comment: ""), icon: "marge-library")
        btnViewMarge.setDeselectValue(title: NSLocalizedString("View", comment: ""), icon: "view-library")
        btnSortMove.setSelectValue(title: NSLocalizedString("Move", comment: ""), icon: "sort-library")
        btnSortMove.setDeselectValue(title: NSLocalizedString("Sort", comment: ""), icon: "move-library")
        btnFolderDelete.setSelectValue(title: NSLocalizedString("Delete", comment: ""), icon: "delete-library")
        btnFolderDelete.setDeselectValue(title: NSLocalizedString("Folder", comment: ""), icon: "folder-library")
        btnSettingShare.setSelectValue(title: NSLocalizedString("Share", comment: ""), icon: "share-library")
        btnSettingShare.setDeselectValue(title: NSLocalizedString("Settings", comment: ""), icon: "settings-library")
        
        btnViewMarge.delegate = self
        btnSortMove.delegate = self
        btnFolderDelete.delegate = self
        btnSettingShare.delegate = self
    }
    //isselect mood
    func isSelect(mode : Bool){
        btnViewMarge.isSelected = mode
        btnSortMove.isSelected = mode
        btnFolderDelete.isSelected = mode
        btnSettingShare.isSelected = mode
    }
    
    func createFolderAlert(complete:@escaping(String)-> Void){
        let alert = UIAlertController(title: NSLocalizedString("Create_Folder", comment: ""), message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = NSLocalizedString("Folder_Placeholder", comment: "")
        }
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { action in
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Create", comment: ""), style: .default, handler: { action in
            let textview = alert.textFields?.first
            
            if textview!.text!.isEmpty{
                textview?.shake()
            }else{
                complete(textview!.text!)
            }
            
        }))
        self.present(alert, animated: true) {
            
        }
    }
    func fileMoreAlert(isFile : Bool){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let colorTag = UIAlertAction(title: NSLocalizedString("Color_Tag", comment: ""), style: .default) { action in
            
        }
        let colorTagIcon = UIImage(named: "delete-library")?.resizeCG(size: CGSize(width: 32, height: 32))
        colorTag.setValue(colorTagIcon, forKey: "image")
        colorTag.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let encrypt = UIAlertAction(title: NSLocalizedString("Encrypt", comment: ""), style: .default) { action in
            
        }
        
        encrypt.setValue(colorTagIcon, forKey: "image")
        encrypt.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")

        let rename = UIAlertAction(title: NSLocalizedString("Rename", comment: ""), style: .default) { action in
            
        }
        rename.setValue(colorTagIcon, forKey: "image")
        rename.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let delete = UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .default) { action in
            
        }
        delete.setValue(colorTagIcon, forKey: "image")
        delete.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { action in
            
        }
        
        alert.addAction(colorTag)
        alert.addAction(encrypt)
        alert.addAction(rename)
        alert.addAction(delete)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func sortAlert(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let atoz = UIAlertAction(title: NSLocalizedString("AtoZ", comment: ""), style: .default) { action in
            
        }
        let atozIcon = UIImage(named: "delete-library")?.resizeCG(size: CGSize(width: 32, height: 32))
        atoz.setValue(atozIcon, forKey: "image")
        atoz.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let ztoa = UIAlertAction(title: NSLocalizedString("ZtoA", comment: ""), style: .default) { action in
            
        }
        
        ztoa.setValue(atozIcon, forKey: "image")
        ztoa.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")

        let newest = UIAlertAction(title: NSLocalizedString("Newest", comment: ""), style: .default) { action in
            
        }
        newest.setValue(atozIcon, forKey: "image")
        newest.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let oldest = UIAlertAction(title: NSLocalizedString("Oldest", comment: ""), style: .default) { action in
            
        }
        oldest.setValue(atozIcon, forKey: "image")
        oldest.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { action in
            
        }
        
        alert.addAction(atoz)
        alert.addAction(ztoa)
        alert.addAction(newest)
        alert.addAction(oldest)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func shareAlert() {
        let alert = UIAlertController(title: "\n\n\n\n", message: nil, preferredStyle: .actionSheet)
        let topView = ShareTopView(frame: CGRect(x: 0, y: 0, width: alert.view.bounds.width-16, height: 100))
        alert.view.addSubview(topView)
        
        let email = UIAlertAction(title: NSLocalizedString("Email", comment: ""), style: .default) { action in
            
        }
        let emailIcon = UIImage(named: "delete-library")?.resizeCG(size: CGSize(width: 32, height: 32))
        email.setValue(emailIcon, forKey: "image")
        email.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let print = UIAlertAction(title: NSLocalizedString("Print", comment: ""), style: .default) { action in
            
        }
        
        print.setValue(emailIcon, forKey: "image")
        print.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")

        let gallerySave = UIAlertAction(title: NSLocalizedString("SaveToGallery", comment: ""), style: .default) { action in
            
        }
        gallerySave.setValue(emailIcon, forKey: "image")
        gallerySave.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let savetofile = UIAlertAction(title: NSLocalizedString("SaveToFile", comment: ""), style: .default) { action in
            
        }
        savetofile.setValue(emailIcon, forKey: "image")
        savetofile.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let more = UIAlertAction(title: NSLocalizedString("More", comment: ""), style: .default) { action in
            
        }
        more.setValue(emailIcon, forKey: "image")
        more.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { action in
            
        }
        
        alert.addAction(email)
        alert.addAction(print)
        alert.addAction(gallerySave)
        alert.addAction(savetofile)
        alert.addAction(more)
        alert.addAction(cancel)
        
        self.present(alert, animated: true) {
            
        }
        
    }
    
    func deleteAlert(complete:@escaping()->Void){
        let alert = UIAlertController(title: NSLocalizedString("Delete_File", comment: ""), message: NSLocalizedString("Delete_Message", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .default, handler: { action in
            complete()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { action in
            
        }))
        
        self.present(alert, animated: true) {
            
        }
    }
    func margeAlert(complete:@escaping()->Void){
        let alert = UIAlertController(title: NSLocalizedString("Are_You_Sure", comment: ""), message: NSLocalizedString("Marge_Message", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Marge", comment: ""), style: .default, handler: { action in
            complete()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { action in
            
        }))
        
        self.present(alert, animated: true) {
            
        }
    }
    
}
extension UIView {
    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
}
