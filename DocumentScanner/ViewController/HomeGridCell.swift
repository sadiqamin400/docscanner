//
//  HomeListCell.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 28/5/21.
//

import UIKit

class HomeGridCell: UICollectionViewCell {

    static let identity = "HomeGridCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        
    }
    
    static func nib()->UINib{
        return UINib(nibName: "HomeGridCell", bundle: nil)
    }
}
