//
//  EditItemCell.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit
import Foundation

class EditItemCell: UICollectionViewCell {
    static let identifire = "EditItemCell"
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.adjustsFontSizeToFitWidth = true
    }

    override func prepareForReuse() {
        lblTitle.text = ""
        imageView.image = nil
    }
    static func nib()->UINib{
        return UINib(nibName: identifire, bundle: nil)
    }
    
    func setData(model : EditItem){
        lblTitle.text = model.name
        if let img = UIImage(named: model.icon){
            imageView.image = img
        }
    }
}
