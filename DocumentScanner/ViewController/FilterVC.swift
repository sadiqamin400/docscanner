//
//  FilterVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 4/6/21.
//

import UIKit

class FilterVC: UIViewController {

    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDone: UIButton!
   
    @IBOutlet weak var btnUndoBrightness: UIButton!
    @IBOutlet weak var btnUndoSharpen: UIButton!
    @IBOutlet weak var btnUndoContrast: UIButton!
    @IBOutlet weak var stepperBrightness: UIStepper!
    @IBOutlet weak var stepperSharpen: UIStepper!
    @IBOutlet weak var stepperContrast: UIStepper!
    @IBOutlet weak var lblBrightness: UILabel!
    @IBOutlet weak var lblSharpen: UILabel!
    @IBOutlet weak var lblContrast: UILabel!
    
    @IBOutlet weak var highlightView: HighlightView!
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: "FilterVC", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        highlightView.setItems(items: [NSLocalizedString("Original", comment: ""),NSLocalizedString("B&W", comment: ""),NSLocalizedString("GrayScale", comment: ""),NSLocalizedString("Contrast", comment: ""),])
        
        // Do any additional setup after loading the view.
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func onBrightnessChange(_ sender: UIStepper) {
    }
    @IBAction func onSharpenChange(_ sender: UIStepper) {
    }
    @IBAction func onContrastChange(_ sender: UIStepper) {
    }
    
    @IBAction func onUndoPressed(_ sender: UIButton) {
    }
    

    @IBAction func onFilterPressed(_ sender: UIButton) {
        
        
    }
    
    
    @IBAction func onDonePressed(_ sender: Any) {
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
}
