//
//  HighlightView.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 6/6/21.
//

import UIKit

class HighlightView: UIView {
    private var contentView = UIView()
    private var allButton :[UIButton] = []
    private var highlightView : UIView = UIView()
    private var space : CGFloat = 20
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    

    func setItems(items : [String]){
        var x : CGFloat = 10
        for i in items{
            let lbl = UILabel()
            lbl.text = i
            lbl.sizeToFit()
            let btn = UIButton(frame: CGRect(x: x, y: 0, width: lbl.bounds.width, height: 30))
            btn.setTitle(i, for: .normal)
            btn.titleLabel?.adjustsFontSizeToFitWidth = true
            allButton.append(btn)
            x = x + lbl.bounds.width + space
            print("X val ",x)
        }
        x = x - space / 2
        contentView.frame = CGRect(origin: .zero, size: CGSize(width: x, height: self.bounds.height))
        contentView.backgroundColor = UIColor(named: "black")
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = self.bounds.height / 2
        addSubview(contentView)
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.widthAnchor.constraint(equalToConstant: x).isActive = true
        contentView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        contentView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        let btn = allButton.first!.bounds
        
        highlightView.frame = CGRect(origin: .zero, size: CGSize(width: btn.width + 20, height: btn.height))
        highlightView.backgroundColor = UIColor.blue
        highlightView.clipsToBounds = true
        highlightView.layer.cornerRadius = self.bounds.height / 2
        contentView.addSubview(highlightView)
        
        for btn in allButton{
            contentView.addSubview(btn)
            btn.addTarget(self, action: #selector(onButtonPressed(sender:)), for: .touchDown)
        }
        self.layoutIfNeeded()
    }
    
    @objc func onButtonPressed(sender : UIButton){
        
        UIView.animate(withDuration: 0.3) {
            self.highlightView.frame = CGRect(x: sender.frame.origin.x - self.space / 2, y: 0, width: sender.bounds.width + self.space , height: sender.bounds.height)
            self.layoutIfNeeded()
        }
    }
}
