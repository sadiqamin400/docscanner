//
//  DocEditVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit

class DocEditVC: UIViewController {
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var lblPages: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagerView: FSPagerView!
    
    var images : [URL] = []
    var imageModelList : [ImageModel] = []
    
    lazy var editList = DataLoader.shared.docEditItems()
    
    private var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        collectionViewSetup()
        // Do any additional setup after loading the view.
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        pagerView.clipsToBounds = false
        pagerView.layer.shadowColor = UIColor.gray.cgColor
        pagerView.layer.shadowOffset = .zero
        pagerView.layer.shadowRadius = 2
        pagerView.layer.shadowOpacity = 0.6
        
        lblPages.text = "\(NSLocalizedString("Page", comment: "")) 1/\(images.count)"
        
        
        for i in images{
            imageModelList.append(ImageModel(image: i))
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onSegmentChange(_ sender: Any) {
        
    }
    @IBAction func onSharePressed(_ sender: Any) {
        
    }
    
    @IBAction func onDonePressed(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
}

extension DocEditVC : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = editList[indexPath.row]
        
        switch obj.type {
        case .Add:
            break
        case .Sign:
            let story = UIStoryboard(name: "Main", bundle: nil)
            let sign = story.instantiateViewController(withIdentifier: "SignatureVC") as? SignatureVC
            sign?.editModel = imageModelList[indexPath.row]
            let nev = UINavigationController.init(rootViewController: sign!)
            nev.modalPresentationStyle = .overCurrentContext
            self.present(nev, animated: true) {
                
            }
            break
        case .Text:
            let text = TextVC(nibName: "TextVC", bundle: nil)
            text.modalPresentationStyle = .fullScreen
            self.present(text, animated: true) {
                
            }
            break
        case .Draw:
            let draw = DrawVC(nibName: "DrawVC", bundle: nil)
            draw.modalPresentationStyle = .fullScreen
            self.present(draw, animated: true) {
                
            }
            break
        case .Highlight:
            let highlight = HighlightVC(nibName: "HighlightVC", bundle: nil)
            highlight.modalPresentationStyle = .fullScreen
            self.present(highlight, animated: true) {
                
            }
            break
        case .Filter:
            let filter = FilterVC(nibName: "", bundle: nil)
            filter.modalPresentationStyle = .fullScreen
            self.present(filter, animated: true) {
                
            }
            break
        case .More:
            break
        
        }
    }
}

extension DocEditVC : FSPagerViewDataSource{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return images.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let data = try! Data(contentsOf: images[index])
        cell.imageView?.image = UIImage(data: data)
        
        return cell
    }
    
    
}
extension DocEditVC : FSPagerViewDelegate{
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        currentIndex = targetIndex + 1
        lblPages.text = "\(NSLocalizedString("Page", comment: "")) \(currentIndex)/\(images.count)"
    }
    
}
