//
//  ShareTopView.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 29/5/21.
//

import UIKit

class ShareTopView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblFree: UILabel!
    @IBOutlet weak var segment: UISegmentedControl!
    
    var shareType : (Int)->Void = {_ in}
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("ShareTopView", owner: self, options: [:])
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentView)
        
        segment.setTitle(NSLocalizedString("PDF", comment: ""), forSegmentAt: 0)
        segment.setTitle(NSLocalizedString("JPG", comment: ""), forSegmentAt: 1)
        
        lblFree.text = NSLocalizedString("Daily_Free_Share", comment: "")
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 5
        imageView.layer.shadowColor = UIColor.gray.cgColor
        imageView.layer.shadowOffset = .zero
        imageView.layer.shadowRadius = 2
        imageView.layer.shadowOpacity = 0.8
    }
    @IBAction func onSegmentChange(_ sender: Any) {
        shareType(segment.selectedSegmentIndex)
    }
}
