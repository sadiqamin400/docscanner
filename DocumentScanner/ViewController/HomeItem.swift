//
//  HomeItem.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 27/5/21.
//

import UIKit

protocol HomeItemDelegate : NSObjectProtocol {
    func onSelected(sender : HomeItem)
    func onDeselected(sender : HomeItem)
}

class HomeItem: UIView {

    weak var delegate : HomeItemDelegate?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClick: UIButton!
    
    private var selectTitle : String = ""
    private var selectIcon : String = ""
    private var deselectTitle : String = ""
    private var deSelectIcon : String = ""
    
    var isSelected : Bool = false{
        didSet{
            if isSelected{
                lblTitle.text = selectTitle
                if let img = UIImage(named: selectIcon){
                    ivImage.image = img
                }
            }else{
                lblTitle.text = deselectTitle
                if let img = UIImage(named: deSelectIcon){
                    ivImage.image = img
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed(String(describing: HomeItem.self), owner: self, options: [:])
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentView)
        
        lblTitle.adjustsFontSizeToFitWidth = true
    }
   
    func setSelectValue(title : String,icon : String) {
        self.selectTitle = title
        self.selectIcon = icon
        self.isSelected = true
    }
    
    func setDeselectValue(title : String,icon : String) {
        self.deselectTitle = title
        self.deSelectIcon = icon
        self.isSelected = false
    }
    
    @IBAction func onClick(_ sender: Any) {
        if isSelected{
            if let delegate = delegate{
                delegate.onSelected(sender: self)
            }
        }else{
            if let delegate = delegate{
                delegate.onDeselected(sender: self)
            }
        }
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
