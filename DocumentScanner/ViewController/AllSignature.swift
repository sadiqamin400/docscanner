//
//  AllSignature.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 3/6/21.
//

import UIKit

class AllSignature: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var imageUrl : [File] = DataLoader.shared.getAllSignature()
    
    var onSelectSign : (File)-> Void = {_ in}
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override var isHidden: Bool{
        didSet{
            if !isHidden{
                imageUrl = DataLoader.shared.getAllSignature()
                tableView.reloadData()
                print(imageUrl.count )
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("AllSignature", owner: self, options: [:])
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentView)
        
        tableView.register(SignatureCell.nib(), forCellReuseIdentifier: SignatureCell.identifire)
        tableView.tableFooterView = UIView()
    }

}

extension AllSignature: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageUrl.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SignatureCell.identifire, for: indexPath) as? SignatureCell else {
            fatalError("cell load error")
        }
        
        do{
            let data = try Data(contentsOf: imageUrl[indexPath.row].url)
            if let img = UIImage(data: data){
                cell.ivImage.image = img
            }
            
        }catch{
            print(error.localizedDescription)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let file = imageUrl[indexPath.row]
            imageUrl.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            let res = FileUtility.shared.deleteFile(url: file.url)
            print("File Delete : ",res)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
}
extension AllSignature : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file = imageUrl[indexPath.row]
        onSelectSign(file)
    }
}
