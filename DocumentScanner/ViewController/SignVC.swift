//
//  SignVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit

class SignVC: UIViewController {

    weak var delegate : SignDelegate?
    
    @IBOutlet weak var btnCancel: UIBarButtonItem!
    lazy var rightButton = UIBarButtonItem(title: NSLocalizedString("Save", comment: ""), style: .plain, target: self, action: #selector(onSavePressed))
    @IBOutlet weak var allSignatureView: AllSignature!
    @IBOutlet weak var signatureView: DrawView!
    
    var segmentControl : UISegmentedControl?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSegmentController()
        //tableViewSetup()
        // Do any additional setup after loading the view.
        
        allSignatureView.onSelectSign = {sing in
            if let delegate = self.delegate{
                self.dismiss(animated: true) {
                    delegate.onSignSelect(file: sing)
                }
            }
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func onSegmentChange(sender : Any){
        if segmentControl?.selectedSegmentIndex == 0{
            allSignatureView.isHidden = true
            signatureView.isHidden = false
            showHideRightBarButton(isHide: false)
            
        }else{
            signatureView.isHidden = true
            allSignatureView.isHidden = false
            showHideRightBarButton(isHide: true)
        }
    }
    
    @objc func onSavePressed(_ sender: Any) {
        let fileName = "\(Int(Date().timeIntervalSince1970 * 1000)).png"
        let img = signatureView.getSignature()
        let data = img?.pngData()
        let url = kSignatureFolder.appendingPathComponent(fileName)
        do{
            try data?.write(to: url)
        }catch{
            print(error.localizedDescription)
        }
        
    }
    
    @IBAction func onCanclePressed(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
}
