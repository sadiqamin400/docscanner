//
//  DrawView.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import UIKit
import SwiftSignatureView

class DrawView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var signatureView: SwiftSignatureView!
    @IBOutlet weak var signatureContentView: UIView!
    private var shadowLayer: CAShapeLayer = CAShapeLayer()
    
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnRed: UIButton!
    @IBOutlet weak var btnBlue: UIButton!
    @IBOutlet weak var btnBlack: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("DrawView", owner: self, options: [:])
        self.contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentView)
        
        btnRed.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnBlue.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnClear.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        shadowLayer.path = UIBezierPath(roundedRect: signatureContentView.bounds, cornerRadius: 10).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor
        shadowLayer.cornerRadius = 10
        shadowLayer.shadowColor = UIColor.gray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = .zero //CGSize(width: 2.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.6
        shadowLayer.shadowRadius = 1

        
        shadowLayer.removeFromSuperlayer()
        signatureContentView.layer.insertSublayer(shadowLayer, at: 0)
        
        
        
        
        
        
    }
    
    @IBAction func onBlackSelect(_ sender: Any) {
        btnBlack.transform = CGAffineTransform(scaleX: 1, y: 1)
        btnRed.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnBlue.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnClear.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        signatureView.strokeColor = UIColor.black
    }
    @IBAction func onBlueSelect(_ sender: Any) {
        btnBlack.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnRed.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnBlue.transform = CGAffineTransform(scaleX: 1, y: 1)
        btnClear.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        signatureView.strokeColor = UIColor.blue
    }
    @IBAction func onRedSelect(_ sender: Any) {
        btnBlack.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnRed.transform = CGAffineTransform(scaleX: 1, y: 1)
        btnBlue.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnClear.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        
        signatureView.strokeColor = UIColor.red
    }
    
    @IBAction func onDeleteSelect(_ sender: Any) {
        btnBlack.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnRed.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnBlue.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        btnClear.transform = CGAffineTransform(scaleX: 1, y: 1)
        
        signatureView.clear()
    }
}

extension DrawView{
    func getSignature() -> UIImage? {
        return signatureView.getCroppedSignature()
        //return signatureView.signature
    }
}
