//
//  ScanVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 29/5/21.
//

import UIKit
import AVFoundation
import Photos
import MobileCoreServices

class ScanVC: UIViewController {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var flashView: UIView!
    @IBOutlet weak var btnAuto: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var imagePreviewView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var lblImageCount: UILabel!
    @IBOutlet weak var preView: UIView!
    

    private var captureSessionManager: CaptureSessionManager?
    private let videoPreviewLayer = AVCaptureVideoPreviewLayer()
    
    /// The view that shows the focus rectangle (when the user taps to focus, similar to the Camera app)
    private var focusRectangle: FocusRectangleView!
    
    /// The view that draws the detected rectangles.
    private let quadView = QuadrilateralView()
    
    /// Whether flash is enabled
    private var flashEnabled = false
    
    private var images : [URL] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preView.isHidden = true
        // Do any additional setup after loading the view.
        captureSessionManager = CaptureSessionManager(videoPreviewLayer: videoPreviewLayer)
        captureSessionManager?.delegate = self
        
        view.layer.insertSublayer(videoPreviewLayer, at: 0)
        quadView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(quadView, at: 1)
        
        let quadViewConstraints = [
            quadView.topAnchor.constraint(equalTo: view.topAnchor),
            view.bottomAnchor.constraint(equalTo: quadView.bottomAnchor),
            view.trailingAnchor.constraint(equalTo: quadView.trailingAnchor),
            quadView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ]
        NSLayoutConstraint.activate(quadViewConstraints)
        
        NotificationCenter.default.addObserver(self, selector: #selector(subjectAreaDidChange), name: Notification.Name.AVCaptureDeviceSubjectAreaDidChange, object: nil)
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        quadView.hideCornerView(value: true)
        CaptureSession.current.isEditing = false
        quadView.removeQuadrilateral()
        captureSessionManager?.start()
        UIApplication.shared.isIdleTimerDisabled = true
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        videoPreviewLayer.frame = view.layer.bounds
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        
        
        captureSessionManager?.stop()
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        if device.torchMode == .on {
            _ = CaptureSession.current.toggleFlash()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

    @IBAction func onGalleryPressed(_ sender: Any) {
    }
    @IBAction func onAutoFlashPressed(_ sender: Any) {
        btnAuto.setTitleColor(UIColor(named: "scanDeselectTextColor"), for: .normal)
        if flashEnabled{
            _ = CaptureSession.current.toggleFlash()
        }
        flashEnabled = false
        
    }
    
    @IBAction func onFlashPressed(_ sender: Any) {
        btnFlash.setTitleColor(UIColor(named: "scanSelectTextColor"), for: .normal)
        if !flashEnabled{
            _ = CaptureSession.current.toggleFlash()
        }
        flashEnabled = true
    }
    
    @IBAction func onCapturePressed(_ sender: Any) {
        captureSessionManager?.capturePhoto()
    }
    @IBAction func onBackPressed(_ sender: Any) {
//        self.dismiss(animated: true) {
//
//        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onDocEditPressed(_ sender: Any) {
        let edit = DocEditVC(nibName: "DocEditVC", bundle: nil)
        edit.images = images
        self.navigationController?.pushViewController(edit, animated: true)
        
    }
}
//MARK:--
extension ScanVC{
    @objc private func subjectAreaDidChange() {
        /// Reset the focus and exposure back to automatic
        do {
            try CaptureSession.current.resetFocusToAuto()
        } catch {
            let error = ImageScannerControllerError.inputDevice
            guard let captureSessionManager = captureSessionManager else { return }
            captureSessionManager.delegate?.captureSessionManager(captureSessionManager, didFailWithError: error)
            return
        }
        
        /// Remove the focus rectangle if one exists
        CaptureSession.current.removeFocusRectangleIfNeeded(focusRectangle, animated: true)
    }
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "Access Denied",
            message: "To scan documents the app needs access to your Camera ",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Access", style: .default, handler: { (alert) -> Void in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        guard  let touch = touches.first else { return }
        let touchPoint = touch.location(in: view)
        let convertedTouchPoint: CGPoint = videoPreviewLayer.captureDevicePointConverted(fromLayerPoint: touchPoint)
        
        CaptureSession.current.removeFocusRectangleIfNeeded(focusRectangle, animated: false)
        
        focusRectangle = FocusRectangleView(touchPoint: touchPoint)
        //  view.addSubview(focusRectangle)
        
        do {
            try CaptureSession.current.setFocusPointToTapPoint(convertedTouchPoint)
        } catch {
            let error = ImageScannerControllerError.inputDevice
            guard let captureSessionManager = captureSessionManager else { return }
            captureSessionManager.delegate?.captureSessionManager(captureSessionManager, didFailWithError: error)
            return
        }
    }
    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: print("nafhlhdlhlhal")
        case .denied: alertToEncourageCameraAccessInitially()
        case .notDetermined: alertToEncourageCameraAccessInitially()
        default: alertToEncourageCameraAccessInitially()
        }
    }
    // MARK: - Actions
    
    private func captureImage() {
        
        self.checkCamera()
        if Store.sharedInstance.isAlreadyScanPage
        {
            return
        }
        captureSessionManager?.capturePhoto()
        
    }
    //MARK:-- auto scan
    @objc private func toggleAutoScan() {
        if CaptureSession.current.isAutoScanEnabled {
            
            CaptureSession.current.isAutoScanEnabled = false
            
        } else {
            
            CaptureSession.current.isAutoScanEnabled = true
        }
    }
    
    
}

//MARK: Capture delegate
extension ScanVC : RectangleDetectionDelegateProtocol{
    func captureSessionManager(_ captureSessionManager: CaptureSessionManager, didFailWithError error: Error) {
        
//        activityIndicator.stopAnimating()
//        shutterButton.isUserInteractionEnabled = true
        
        
    }
    
    func didStartCapturingPicture(for captureSessionManager: CaptureSessionManager) {
        //activityIndicator.startAnimating()
        captureSessionManager.stop()
        //shutterButton.isUserInteractionEnabled = false
    }
    
    func captureSessionManager(_ captureSessionManager: CaptureSessionManager, didCapturePicture picture: UIImage, withQuad quad: Quadrilateral?) {
        //activityIndicator.stopAnimating()
        
        if Store.sharedInstance.isAlreadyScanPage
        {
            return
        }
       
        let editVC = EditScanVC(image: picture, quad: quad)
        editVC.delegate = self
        editVC.modalPresentationStyle = .fullScreen
        self.present(editVC, animated: true) {
            
        }

//        shutterButton.isUserInteractionEnabled = true
    }
    
    func captureSessionManager(_ captureSessionManager: CaptureSessionManager, didDetectQuad quad: Quadrilateral?, _ imageSize: CGSize) {
        guard let quad = quad else {
            // If no quad has been detected, we remove the currently displayed on on the quadView.
            quadView.removeQuadrilateral()
            return
        }
        
        let portraitImageSize = CGSize(width: imageSize.height, height: imageSize.width)
        
        let scaleTransform = CGAffineTransform.scaleTransform(forSize: portraitImageSize, aspectFillInSize: quadView.bounds.size)
        let scaledImageSize = imageSize.applying(scaleTransform)
        
        let rotationTransform = CGAffineTransform(rotationAngle: CGFloat.pi / 2.0)
        
        let imageBounds = CGRect(origin: .zero, size: scaledImageSize).applying(rotationTransform)
        
        let translationTransform = CGAffineTransform.translateTransform(fromCenterOfRect: imageBounds, toCenterOfRect: quadView.bounds)
        
        let transforms = [scaleTransform, rotationTransform, translationTransform]
        
        let transformedQuad = quad.applyTransforms(transforms)
        
        quadView.drawQuadrilateral(quad: transformedQuad, animated: true)
    }
}

extension ScanVC : CropDelegate{
    
    
    func onCropError() {
        print("Crop Error")
    }
    
    func onCropDone(url: URL) {
        images.append(url)
        
        let data = try! Data(contentsOf: url)
        imagePreview.image = UIImage(data: data)
        
        lblImageCount.text = "\(images.count)"
        preView.isHidden = false
        
    }
}
