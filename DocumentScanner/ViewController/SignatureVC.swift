//
//  SignatureVC.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 8/6/21.
//

import UIKit
import AVFoundation

protocol SignDelegate : NSObjectProtocol {
    func onSignSelect(file : File)
}

class SignatureVC: UIViewController {
    
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnCancel: UIBarButtonItem!
    @IBOutlet weak var btnDone: UIBarButtonItem!
    
    private var editView : ImageEditView?
    
    var editModel : ImageModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if editView == nil{
            let data = try! Data(contentsOf: editModel!.image)
            let img = UIImage(data: data)
            let rect = AVMakeRect(aspectRatio: img!.size, insideRect: contentView.bounds)
            
            editView = ImageEditView(frame: rect)
            
            editView?.setModel(model: editModel!)
            self.contentView.addSubview(editView!)
        }
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let sign = story.instantiateViewController(withIdentifier: "SignVC") as? SignVC
        sign?.delegate = self
        let nev = UINavigationController.init(rootViewController: sign!)
        nev.modalPresentationStyle = .overCurrentContext
        self.present(nev, animated: true) {
            
        }
   
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
   
    @IBAction func onCancelPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func onDonePressed(_ sender: Any) {
    }
}

extension SignatureVC : SignDelegate{
    func onSignSelect(file: File) {
        let data = try! Data(contentsOf: file.url)
        let img = UIImage(data: data)
        
        let rect = AVMakeRect(aspectRatio: img!.size, insideRect: CGRect(origin: .zero, size: CGSize(width: 150, height: 100)))
        
        let jrs = JRStickerView(frame: CGRect(origin: CGPoint(x: editView!.frame.midX, y: editView!.frame.midY), size: rect.size))
        let iv = UIImageView()
        iv.image = img
        jrs.setContentView(iv)
        jrs.stickerViewDelegate = self
        jrs.parentFrame = editView!.bounds
        editView!.addSubview(jrs)
        
        editModel?.signature = SignatureModel(image: img, parent: editView!.bounds, child: jrs.frame)
    }
}

extension SignatureVC : ZDStickerViewDelegate{
    func stickerViewDidBeginEditing(sticker: JRStickerView?) {
        
    }
    func stickerViewDidEndEditing(sticker: JRStickerView?) {
        editModel?.signature.child = sticker!.frame
        
    }
    func stickerViewDidClose(sticker: JRStickerView?) {
        editModel?.signature.image = nil
    }
}
