//
//  UIButton+Extension.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 27/5/21.
//

import Foundation
import UIKit
extension UIButton {
    /// 0 => .ScaleToFill
    /// 1 => .ScaleAspectFit
    /// 2 => .ScaleAspectFill
    @IBInspectable
    var imageContentMode: Int {
        get {
            return self.imageView?.contentMode.rawValue ?? 0
        }
        set {
            if let mode = UIView.ContentMode(rawValue: newValue),
                self.imageView != nil {
                self.imageView?.contentMode = mode
            }
            layoutIfNeeded()
        }
    }
}

