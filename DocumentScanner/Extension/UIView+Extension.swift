//
//  UIView+Extension.swift
//  DocumentScanner
//
//  Created by Azizur Rahman on 2/6/21.
//

import Foundation
import UIKit

extension UIView{
    @IBInspectable
    var cornarRadius : CGFloat{
        get {
            return self.layer.cornerRadius
        }set{
            self.clipsToBounds = true
            self.layer.cornerRadius = newValue
            layoutIfNeeded()
        }
    }
    
}
